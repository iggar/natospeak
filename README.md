# natospeak
## Spell a string based on the NATO phonetic alphabet.

This is a command line script that receives a string as an input. It will split the string into letters and play out audio with the equivalent words for each letter following the [NATO phonetic alphabet](https://www.nato.int/nato_static_fl2014/assets/pictures/stock_2018/20180110_alphabet-sign-signal-big2.jpg)

### Requirements

You need [VLC](http://www.videolan.org/) installed for this script to work.

### Limitations

Only lowercase letters are accepted.

### Usage

```bash
./say.sh <string>
```

Example:

```bash
./say.sh nato
```

(it should play "november alpha tango oscar")

