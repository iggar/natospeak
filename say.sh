#!/bin/bash

read -a theword <<< $(echo $1 | sed 's/./&\n/g')

for letter in "${theword[@]}"; do
        voicefilename=./resources/${letter}.wav
        cvlc -q --no-repeat --play-and-exit $voicefilename &> /dev/null
done

